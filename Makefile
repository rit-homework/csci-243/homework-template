CFLAGS=-Wall -Wextra -pedantic -std=c99

all: main

# TODO: Target and Source File Names

main: main.o
	$(CC) -o main main.o

clean:
	rm main{,.o}

.PHONY: clean
